package com.demo.example;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class DemoTest {
	
	@ParameterizedTest
	@ValueSource(strings = {"madam", "racecar", "radar", "aba"})
	public void isPalindromeTest(String s) {
		Demo demo = new Demo();
		boolean flag = demo.isPalindrome(s);
		assertTrue(flag);
	}

}
